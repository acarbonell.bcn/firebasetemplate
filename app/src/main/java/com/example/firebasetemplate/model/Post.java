package com.example.firebasetemplate.model;

import android.net.Uri;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;

import java.util.HashMap;
import java.util.List;

public class Post {
    public String id;
    public String content;
    public String authorNme;
    public String date;
    public String imageUrl;
    public List<Comment> comments;


    public Post() {
    }

    public HashMap<String, Boolean> likes = new HashMap<>();
}
