package com.example.firebasetemplate;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentPostsBinding;
import com.example.firebasetemplate.databinding.FragmentProfileBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;


public class ProfileFragment extends AppFragment {
    private FragmentProfileBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentProfileBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //navController.navigate(R.id.action_profileFragment_to_navigation);
        FirebaseAuth.getInstance().addAuthStateListener(firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() != null) {
                if (firebaseAuth.getCurrentUser().getPhotoUrl() != null){
                    Glide.with(this).load(firebaseAuth.getCurrentUser().getPhotoUrl()).circleCrop().into(binding.photo);
                }

                binding.userName.setFocusable(false);
                binding.userName.setText(firebaseAuth.getCurrentUser().getDisplayName());
                binding.userEmail.setFocusable(false);
                binding.userEmail.setText(firebaseAuth.getCurrentUser().getEmail());
                binding.userPassword.setFocusable(false);
                binding.userPassword.setText("********");
                binding.btnSave.setEnabled(false);

                Log.e("sdfdfs","USER:" + firebaseAuth.getCurrentUser().getEmail());

                binding.btnUpdate.setOnClickListener(v -> {
                    binding.userName.setFocusable(true);
                    binding.userName.getEditableText();
                    binding.btnSave.setAlpha(1);
                    binding.btnSave.setEnabled(true);

                });

                binding.btnSave.setOnClickListener(v -> {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(binding.userName.getText().toString()).build();
                    user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d("TAG", "User profile updated.");
                            }
                        }
                    });
                });
            }

        });

    }


}