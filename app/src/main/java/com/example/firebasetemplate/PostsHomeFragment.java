package com.example.firebasetemplate;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.CommentPostBinding;
import com.example.firebasetemplate.databinding.FragmentPostsBinding;
import com.example.firebasetemplate.databinding.ViewholderPostBinding;
import com.example.firebasetemplate.model.Comment;
import com.example.firebasetemplate.model.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;

public class PostsHomeFragment extends AppFragment {

    private FragmentPostsBinding binding;
    private List<Post> postList = new ArrayList<>();
    //private List<Comment> commList = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentPostsBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.fab.setOnClickListener(v -> navController.navigate(R.id.newPostFragment));

        PostAdapter adapter;
        binding.postsRecyclerView.setAdapter(adapter = new PostAdapter());

        setQuery().addSnapshotListener((collectionSnapshot, e) -> {
            postList.clear();
            for (DocumentSnapshot documentSnapshot: collectionSnapshot){
                Post post = documentSnapshot.toObject(Post.class);
                post.id = documentSnapshot.getId();
                postList.add(post);
            }
            adapter.notifyDataSetChanged();
        });

    }

    Query setQuery(){
        return db.collection("posts");
    }


    class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder>{
        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(ViewholderPostBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Post post = postList.get(position);
            holder.binding.contenido.setText(post.content);
            holder.binding.autor.setText(post.authorNme);
            Glide.with(requireContext()).load(post.imageUrl).into(holder.binding.imagen);
           /* holder.binding.itemPost.setOnClickListener(v -> {
                navController.navigate(R.id.action_global_detailFragment);
            });*/
            holder.binding.numFav.setText(String.valueOf(post.likes.size()));
            //holder.binding.numComment.setText(db.collection("posts").document("qwerty").collection("comment").document().len);
            holder.binding.favorito.setOnClickListener(v -> {
                db.collection("posts").document(post.id).update("likes." + auth.getUid(), !post.likes.containsKey(auth.getUid()) ? true : FieldValue.delete());
                Bundle bundle = new Bundle();
                bundle.putString("id", db.collection("posts").document(post.id).getId());
                Toast.makeText(requireContext(), db.collection("posts").document(post.id).getId(), Toast.LENGTH_SHORT).show();
            });
            holder.binding.favorito.setChecked(post.likes.containsKey(auth.getUid()));
            holder.binding.addComment.setOnClickListener(v-> {
                Bundle bundle = new Bundle();
                bundle.putString("id", db.collection("posts").document(post.id).getId());
                navController.navigate(R.id.action_postsHomeFragment_to_newCommentFragment, bundle);
            });
            List<Comment> commList = new ArrayList<>();
            CommentAdapter adapterCom;
            holder.binding.recyclerComment.setAdapter(adapterCom = new CommentAdapter(requireContext(), commList));

            db.collection("posts").document("qwerty").collection("comment").addSnapshotListener((collectionSnapshot, e) -> {
                commList.clear();
                for (DocumentSnapshot documentSnapshot: collectionSnapshot){
                    Comment comment = documentSnapshot.toObject(Comment.class);
                    comment.idComm = documentSnapshot.getId();
                    commList.add(comment);
                }
                adapterCom.notifyDataSetChanged();
            });

        }

        @Override
        public int getItemCount() {
            return postList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ViewholderPostBinding binding;
            public ViewHolder(ViewholderPostBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
    class CommentAdapter extends  RecyclerView.Adapter<CommentAdapter.ViewHolder>{

        Context context;
        List<Comment> commList = new ArrayList<>();

        public CommentAdapter(Context context, List<Comment> commList) {
            this.context = context;
            this.commList = commList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(CommentPostBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Comment comment = commList.get(position);
            //holder.binding.textAuthor.setText(comment.authorNme);
            holder.binding.textComm.setText(comment.comment);
            holder.binding.textData.setText(comment.date);
        }

        @Override
        public int getItemCount() {
            return commList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CommentPostBinding binding;
            public ViewHolder(CommentPostBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }


}