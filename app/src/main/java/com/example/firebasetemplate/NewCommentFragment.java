package com.example.firebasetemplate;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.firebasetemplate.databinding.FragmentNewCommentBinding;
import com.example.firebasetemplate.model.Comment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class NewCommentFragment extends AppFragment {

    private FragmentNewCommentBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return (binding = FragmentNewCommentBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.publicar.setOnClickListener(v -> {
            binding.publicar.setEnabled(false);
            Comment comment = new Comment();
            comment.comment = binding.contenido.getText().toString();
            comment.authorNme = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
            comment.date = getData();
            FirebaseFirestore.getInstance().collection("posts").document(getArguments().getString("id")).collection("comment").add(comment).addOnCompleteListener(task -> {
                binding.publicar.setEnabled(true);
                navController.popBackStack();
            });

        });
    }

    public String getData(){
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yy");
        return  now.format(formatter);

    }
}